﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace amazon.algorithms
{
    public static class Algorithm
    {
        // for checking zero result
        private static readonly (int, int) ZeroPair = (0, 0);

        /// <summary>
        /// get indexes with shortest distance
        /// </summary>
        /// <param name="tags">tags for searching</param>
        /// <param name="allTags">collection to search in</param>
        /// <returns>pair of indexes</returns>
        public static (int, int) CalculateResult(string[] tags, string[] allTags)
        {
            var calculations = new List<(int, int)>();
            for (var index = 0; index < allTags.Length; index++)
            {
                var currentTag = allTags[index];
                // no need to check tag that's not in search
                if (!tags.Contains(currentTag)) continue;

                var calculation = Routin(currentTag, index, tags, allTags);

                if (ZeroPair.Item1 != calculation.Item1
                    && ZeroPair.Item2 != calculation.Item2) calculations.Add(calculation);
            }

            // couldn't find every tag in whole tag collection
            if (!calculations.Any()) return ZeroPair;

            // return the shortest one
            return calculations.OrderBy(x => x.Item2 - x.Item1)
                               .First();
        }

        private static (int, int) Routin(string startingTag, int startIndex, string[] tags, string[] allTags)
        {
            // create dictionary with starting tag and it's index
            var dictionary = new Dictionary<string, int> { { startingTag, startIndex } };
            for (var index = startIndex; index < allTags.Length; index++)
            {
                var currentTag = allTags[index];
                // check if it's in searching tags
                if (tags.Contains(currentTag)
                    // and we don't already have in current search.
                    && !dictionary.ContainsKey(currentTag)) dictionary.Add(currentTag, index);

                // when we found all search tags return pair
                if (dictionary.Count == tags.Length) return (startIndex, index);
            }

            // couldn't find every tag
            return ZeroPair;
        }
    }
}
