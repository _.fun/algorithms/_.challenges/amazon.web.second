using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace amazon.algorithms.test2
{
    [TestClass]
    public class AlgorithmTest
    {
        [TestMethod]
        public void ZeroResult()
        {
            // [arrange]
            var tags = new string[] { "tag1", "tag2", "tag3" };
            var allTags = new string[] { "tag1", "tag4", "tag5", "tag1", "tag1", "tag3", "tag5" };

            // [act]
            var result = Algorithm.CalculateResult(tags, allTags);

            // [assert]
            Assert.AreEqual((0, 0), result);
        }

        [TestMethod]
        public void SmallTagsValid()
        {
            // [arrange]
            var tags = new string[] { "tag1", "tag2", "tag3" };
            var allTags = new string[] { "tag3", "tag1", "tag3", "tag2" };

            // [act]
            var result = Algorithm.CalculateResult(tags, allTags);

            // [assert]
            Assert.AreEqual((1, 3), result);
        }

        [TestMethod]
        public void LargeTagsValid()
        {
            // [arrange]
            var tags = new string[] { "tag1", "tag2", "tag3" };
            var allTags = new string[] { "tag3", "tag4", "tag5", "tag2", "tag1", "tag3", "tag5" };

            // [act]
            var result = Algorithm.CalculateResult(tags, allTags);

            // [assert]
            Assert.AreEqual((3, 5), result);
        }
    }
}
