# The Challenge

## Description

This a second task from Amazon which I couldn't complete in Web IDE (.Net 3.5) and done in 15-30 minutes in VS (C# 7.2).

### Task

You need to find starting end ending index of tags in a list with shortest distance between each other. In case not all tags are present in the collection, return empty result. 

### Input

- tags: that should be used for finding start and end index in allTags
- allTags: list of different tags that can contain repetition

### Example

Initial input:

``` C#
var tags = new string[] { "tag1", "tag2", "tag3" };
var allTags = new string[] { "tag3", "tag1", "tag3", "tag2" };
```

Output:

``` C#
1, 3
```